Pre req. 

1. Eclipse
2. TestNG plugin (To install TestNG plugin, have a look at "https://www.guru99.com/install-testng-in-eclipse.html" )
3. Chromedriver computable with chrome
Steps to run this project :

 1. After pulling the repo to your local , open it on eclipse 
 2. From Utils package>  Driver Class, change the location of chromedriver to your location
 3. From project explorer , right click on the project and move to Maven>Update Project 
 4. After update, open the project and find the file "Eskimi.xml"
 5. Run this file as TestNG Suite.
 
 